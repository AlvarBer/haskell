Haskell
=======
A repo where I play with Haskell for a while.

Base
----
Where I go and unimport (almost) all the prelude functions and 
reimplement them in order to learn from the basics.

AbstractDatatypes
-----------------
Where I program my (extremely inefficient) Data Structures.

Labs
----
These correspond to the subject Declarative Programming taught in 
Universidad Complutense (*Marker.hs* being the final project).

Lib
---
A few handy functions I use from time to time.

LearnMonad
----------
Here I struggle with Monads.

Brain
-----
An attempt at a Neural Network in Haskell (just for learning).

Todo
----
A quick argument handling program.
