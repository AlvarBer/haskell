module Pair where


{- Because the underlying tree needs a type with complete ordering but we do
 - not want to force both keys and values to be of typeclass Ord we need to
 - define a tuple where only the first element has total ordering. -}
newtype Pair a b = Pair (a, b) deriving (Show)

{- Type unpacking. -}
unPair :: Pair a b -> (a, b)
unPair (Pair x) = x

{- We only care about the first element of the tuple. -}
instance (Eq a) => Eq (Pair a b) where
    (Pair x) == (Pair y) = fst x == fst y

instance (Ord a) => Ord (Pair a b) where
    (Pair x) <= (Pair y) = fst x <= fst y

{- Functor actautes only on the second element. -}
instance Functor (Pair a) where
    fmap f (Pair (x, y)) = Pair (x, f y)

