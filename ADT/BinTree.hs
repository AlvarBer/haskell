{- Module Binary (Search) Tree
 -
 - It seems like a BST as defined on cormen is the same as a TreeMap?
 -
 - -}
module ADT.BinTree (BinTree(Leaf), isEmpty, OrderedList(..)) where

import Interfaces
import Data.List(genericIndex)


{- Simple definition. -}
data BinTree a = Leaf | Node {left :: BinTree a, val :: a, right :: BinTree a}
                              deriving (Eq)

---------- Simple functions ----------
isEmpty :: BinTree a -> Bool
isEmpty Leaf = True
isEmpty _ = False

-- Only makes sense with TreeMap
find :: Ord a => a -> BinTree a -> Maybe a
find e Leaf = Nothing
find e (Node l v r) | e == v = Just e
                    | e < v = find e l
                    | e > v = find e r

---------- Traversals ----------
preorder :: Show a => BinTree a -> String
preorder Leaf = ""
preorder (Node l v r) = show v ++ " " ++ preorder l ++ " " ++ preorder r

inorder :: Show a => BinTree a -> String
inorder Leaf = ""
inorder (Node l v r) = inorder l ++ " " ++ show v ++ " " ++ inorder r

postorder :: Show a => BinTree a -> String
postorder Leaf = ""
postorder (Node l v r) = postorder l ++ " " ++ postorder r ++ " " ++ show v

---------- Typeclasses instances ----------
instance OrderedList BinTree where
        {- Inserts an item keeping the tree sorted. -}
        insert e Leaf = Node Leaf e Leaf
        insert e (Node l v r)
                | e == v = Node l v r
                | e < v = Node (insert e l) v r
                | e > v = Node l v (insert e r)
        {- Traverses a tree until it finds the node to delete. -}
        delete _ Leaf = Leaf
        delete e (Node l v r)
                | e == v = delete' (Node l v r)
                | e /= v = Node (delete e l) v r
        {- Creates a BinaryTree from a list. -}
        fromList = foldr insert Leaf
        {- Exports a BinaryTree to a list. -}
        toList Leaf = []
        toList (Node l v r) = toList l ++ [v] ++ toList r
        {- Gets the maximum in O(h). -}
        fastMax (Node _ v Leaf) = v
        fastMax (Node l v r) = fastMax r
        {- Gets the minimum in O(h). -}
        fastMin (Node Leaf v _) = v
        fastMin (Node l v r) = fastMin l

{- Proper show instance would need a heap. -}
instance (Show a) => Show (BinTree a) where
        show Leaf = "□"
        show (Node l v r) = unlines $ show v : drawTree l "-" ++
                            drawTree r "-"

drawTree :: Show a => BinTree a -> String -> [String]
drawTree Leaf _ = []
drawTree (Node l v r) s = (s ++ " " ++ show v) : drawTree l (s ++ "-") ++
                           drawTree r (s ++ "-")


{- Functor applies the given function to all elements in tree. -}
instance Functor BinTree where
        fmap _ Leaf = Leaf
        fmap f (Node l v r) = Node (fmap f l) (f v) (fmap f r)

{- Applicative does not make too much sense by itself, maybe on traversable?. -}
instance Applicative BinTree where
        pure t = Node (pure t) t (pure t)
        Leaf <*> _ = Leaf
        _ <*> Leaf = Leaf
        (Node l f r) <*> (Node l' v r') = Node (l <*> l') (f v) (r <*> r')

-- Implement foldable

---------- Aux functions ----------
{- Actual deletion function, we know e == v. -}
delete' :: Ord a => BinTree a -> BinTree a
delete' (Node Leaf v Leaf) = Leaf
delete' (Node l v Leaf) = l
delete' (Node Leaf v r) = r
delete' (Node l v r) = Node l (val y) r'
        where y = fastMinTree r
              r' = if right y == Leaf
                           then r
                           else righty r (right y) (val y)

{- This functions returns the node of the minimum element. -}
fastMinTree :: BinTree a -> BinTree a
fastMinTree (Node Leaf v r) = Node Leaf v r
fastMinTree t = fastMinTree (left t)

{- Righty gets two trees and a pivot value and finds the value of the pivot
 - on the first one and subtitues it by the second -}
-- TODO: Cormen function?
righty :: Eq a => BinTree a -> BinTree a -> a -> BinTree a
righty Leaf _ _ = Leaf
righty (Node l v r) r' y
        | val l /= y = Node (righty l r' y) v r
        | val l == y = r'
