module ADT.Interfaces where

import Data.List(genericIndex)

class OrderedList f where
        at :: Integral b => f a -> b -> a
        insert :: Ord a => a -> f a -> f a
        delete :: Ord a => a -> f a -> f a 
        fromList :: Ord a => [a] -> f a
        toList :: f a -> [a]
        fastMax :: f a -> a
        fastMin :: f a -> a
        -- Default implementations
        at f i = toList f `genericIndex` i
        insert e = fromList . (e:) . toList 
        delete e = fromList . filter (\x -> x /= e) . toList
        --fromList = foldr1 insert

class Dictionary d where
        lookup :: d a b -> a -> Maybe b
        keys :: d a b -> [a]
        values :: d a b -> [b]
