module ADT.Interface.List where

import Prelude hiding (head, tail, length, take, drop, splitAt, (!!))

-- Is this equivalent to Foldable?
class List f where
        empty :: f a -- Empty List
        head :: f a -> a
        tail :: f a -> f a
        (<:>) :: a -> f a -> f a  -- Apend operator
        infixr 5 <:>
        (<++>) :: Eq (f a) => f a -> f a -> f a -- Concat operator
        xs <++> ys
                | xs == empty = ys
                | otherwise = head xs <:> (<++>) (tail xs) ys
        length :: (Integral b, Eq (f a)) => f a -> b
        length xs
                | xs == empty = 0
                | otherwise   = 1 + length (tail xs)
        (!!) :: Integral b => f a -> b -> a
        infix 5 !!
        xs !! 0 = head xs
        xs !! n = tail xs !! (n-1)
        take :: Integral a => a -> f b -> f b
        take 0 _ = empty
        take n xs = head xs <:> take (n-1) (tail xs)
        drop :: Integral a => a -> f b -> f b
        drop 0 xs = xs
        drop n xs = drop (n-1) (tail xs)
        splitAt :: Integral a => a -> f b -> (f b, f b)
        splitAt n xs = (take n xs, drop n xs)
        --insertAt :: Integral b => a -> b -> f a -> f a
        --insertAt e p xs = prev <:> e <:> post
        --        where prev, post = splitAt p xs
        --delete :: Integral b => f a -> b -> f a
