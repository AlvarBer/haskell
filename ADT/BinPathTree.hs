{- Module BinPathTree
 -
 - Binary tree based on a list of path implementation -}

 module ADT.BinPathTree where

 data BinPathTree a = [(String, a)]

 ---------- Manipulation ----------
 insert :: Ord a => BinPathTree -> a -> BinPathTree
 insert = undefined
