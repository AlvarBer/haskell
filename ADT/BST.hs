{- Module BST
 -
 - A generic Map (dictionary) that uses a binary tree under the hood.
 -
 - We require the key type to be a member of Ord, and that's it.-}
module ADT.BST where

import qualified BinTree as BT
import Pair
import Interfaces(Dictionary)


{- A treemap is merely a concrete case of a Binary Tree. -}
data BST k v = BST (BT.BinTree (Pair k v)) deriving (Show)

{- Type unpacking. -}
unBST :: BST k v -> BT.BinTree (Pair k v)
unBST (BST m) = m

{- Empty Map is an empty Tree. -}
empty = BST BT.Leaf


{- Get all the keys, O(n). -}
keys :: BST k v -> [k]
keys = fmap fst . toList
-- Equivalent but lower level form
-- keys = BT.toList . fmap (fst . unPair) . unBST

{- Get all the values on the Map, O(n). -}
values :: BST k v -> [v]
values = fmap snd . toList

{- Insert a value on the Map, O(log n). -}
insert :: Ord k => (k, v) -> BST k v -> BST k v
insert (k, v) = BST . BT.insert (Pair (k, v)) . unBST

{- Creates a BST from a list, O(n). -}
fromList :: Ord k => [(k, v)] -> BST k v
fromList = BST . BT.fromList . map Pair

{- Convert the Map into a list, O(n). -}
toList :: BST k v -> [(k, v)]
toList = fmap unPair . BT.toList . unBST

{- This functor instance is a little bit tricky because the Pair type, since
 - we cannot directly access the tuple of the tree instead of unpacking the
 - best thing is to make the Pair type a functor and let it apply the function
 - himself. That is why there are two fmap on the definition, one for the
 - BinTree and another for the Pair. This also allow us to fmap to operate
 - only on values. -}
instance Ord k => Functor (BST k) where
        fmap f = BST . fmap (fmap f) . unBST

