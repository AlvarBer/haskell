module ADT.List where

import qualified ADT.Interface.List as L

data List a = Empty | Cons a (List a) deriving Eq

instance (Show a) => Show (List a) where
        show Empty = "[]"
        show xs = "[" ++ listShow xs ++ "]"

listShow (Cons x Empty) = show x
listShow (Cons x xs) = show x ++ "," ++ listShow xs

instance L.List List where
        empty = Empty
        head (Cons x xs) = x
        tail (Cons x xs) = xs
        x <:> xs = Cons x xs
