module Learn.Monad where

import Labs.Lab3(Complex)
import System.Random

-- You could have invented monads! (and maybe you already have)

-- Let's say we need to debug our functions. In order to do that we
-- need to return a string along the result
square :: Float -> (Float, String)
square x = (x^2, "f: " ++ show x ++ " -> " ++ show (x^2))

quarter :: Float -> (Float, String)
quarter x = (x / 4, "g: " ++ show x ++ " -> " ++ show (x/4))



-- But we changed the signatures, How can we compose them now?
-- We need to transform the second function f
(⊕) :: (Float -> (Float, String)) -> (Float -> (Float, String)) ->
       (Float -> (Float, String))
f ⊕ g = bind f . g

-- bind is where the magic happens, this is the way we compose those
-- functions.
bind :: (Float -> (Float, String)) -> (Float, String) -> (Float, String)
bind f (gx, gs) = (fx, gs ++ " | "  ++ fs)
	where (fx, fs) = f gx

-- unit is the identity that follows f ⊕ unit = unit ⊕ f = f
unit :: Float -> (Float, String)
unit x = (x, "")

-- lift automatically makes a function that wasn't debuggable composable by
-- appling unit
lift :: (Float -> Float) -> Float -> (Float, String)
lift f = unit . f

-- Now we can combine both functions!
h :: Float -> (Float, String)
h = square ⊕ quarter

-- Even compose a debuggable and non-debuggable function
h2 = square ⊕ (lift (/4))

-- And of course we can chain these functions all we want
h3 = quarter ⊕ square ⊕ (lift (/4))

-- Hypothesis: lift f ⊕ lift g = lift (f . g)
-- lift f ⊕ lift g => unit . f ⊕ unit . g
-- => (f, "") ⊕ (g, "") -- We don't need bind this case
-- => (f . g, "") -- But we should have defined another composition
-- => lift (f . g)
-- The annotations are breaks in the Haskell Type System


-- Complex Number examples (Now with Typeclasses)
-- If we want to find the sixth root of a complex number we would like to do
-- something like sixthrootcplx = sqrtcplx ⊕ cbrtcplx, but because each time
-- we get a list of result we need a bind

(<⊕>) :: (Complex -> [Complex]) -> (Complex -> [Complex]) ->
         (Complex -> [Complex])
f <⊕> g = bind' f . g

bind' :: (Complex -> [Complex]) -> [Complex] -> [Complex]
bind' f gx = concat (map f gx)

unit' :: Complex -> [Complex]
unit' x = [x]

sqrtcplx :: Complex -> [Complex]
sqrtcplx c = undefined

cbrtcplx :: Complex -> [Complex]
cbrtcplx c = undefined

strtcplx :: Complex -> [Complex]
strtcplx = sqrtcplx <⊕> cbrtcplx

-- Hypothesis f ⊕ unit = unit ⊕ f = f
-- f ⊕ unit => f ⊕ id [x] => bind f . id [x]
-- => bind f [x] => concat (map f [x])
-- => concat [[fx]] => [fx] => f
-- unit ⊕ f => bind unit . f => bind unit . [fx]
-- => bind unit [fx] => concat (map unit [fx])
-- => concat ([[fx]]) => [fx] => f

-- Hypothesis lift f ⊕ lift g = lift (f . g)
-- lift f ⊕ lift g => [fx] ⊕ [gx] We don't need bind on this case
-- => bind [fx] . [gx] => [fx] . [gx] like last time
-- => [[fgx]] => lift [fgx] => lift (f . g)

-- Random functions
-- Random functions take a seed and return the value we want and the new seed
--randNum :: (RandomGen g, Random a) => Int -> (g -> (a, g))
randNum x = random (mkStdGen x)

divByRand :: a -> StdGen -> (b, StdGen)
divByRand x seed = undefined

-- This time we aren't really making a monad, but a monadic category?
(<<⊕>>) :: (b -> StdGen -> (c, StdGen)) -> (a -> StdGen -> (b, StdGen))
           -> (a -> StdGen -> (c, StdGen))
f <<⊕>> g = bind'' f . g

bind'' :: (a -> StdGen -> (b, StdGen)) -> (StdGen -> (a, StdGen)) -> StdGen ->
          (b, StdGen)
bind'' f rand seed = f x seed'
	where (x, seed') = rand seed

unit'' :: a -> StdGen -> (a, StdGen)
unit'' = (,)

--h'' :: (a -> StdGen -> (c, StdGen))
--h'' = divByRand <<⊕>> randNum
-- f ⊕ g => bind f . g => bind f . (\x -> (\seed -> (gx, StdGen))
--                       (b ->c) . ( a ->         b            )
-- => bind (\x (\seed -> (fx, StdGen))) . (\x -> (\seed -> (gx, StdGen)))
--    (b -> c)                          . (a  ->          b            )
-- => bind


-- Hypothesis f ⊕ unit = unit ⊕ f = f
-- f ⊕ unit => bind f . unit => bind f . (,) rand seed
-- => f rand seed => f
-- unit ⊕ seed

-- Hypothesis lift f * lift g = lift (f.g)

