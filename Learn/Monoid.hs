module Learn.Monoid where

import qualified Prelude as P  -- Hide prelude
import Prelude(Num, Show, Ord,
               (.), ($), (+), (*), (++), (&&), (||),
               Bool(..), Maybe(..))
import Data.Map

class Monoid a where
        mempty :: a -- Neutral element
        mappend :: a -> a -> a -- Commutative Operation
        mappend = (<>)
        mconcat :: [a] -> a -- "Flattening" operator
        mconcat = P.foldr mappend mempty
        (<>) :: a -> a -> a
        (<>) = mappend

newtype Sum a = Sum a deriving Show

instance Num a => Monoid (Sum a) where
        mempty = Sum 0
        Sum x <> Sum y = Sum (x + y)

newtype Mul a = Mul a deriving Show

instance Num a => Monoid (Mul a) where
        mempty = Mul 1
        Mul x <> Mul y = Mul (x * y)

newtype Mand = Mand Bool deriving Show

instance Monoid Mand where
        mempty = Mand True
        Mand x <> Mand y = Mand (x && y)

newtype Mor = Mor Bool deriving Show

instance Monoid Mor where
        mempty = Mor False
        Mor x <> Mor y = Mor (x || y)

instance Monoid [a] where
        mempty = []
        (<>) = (++)

instance Monoid a => Monoid (Maybe a) where
        mempty = mempty :: Monoid a => a
        Nothing <> y = y
        x <> Nothing = x
        Just x <> Just y = Just (x <> y)

instance Ord k => Monoid (Map k v) where
        mempty = empty
        x <> y = fromList $ toList x <> toList y

