module Learn.Base where

import qualified Prelude as P -- Let's hide everything!
import GHC.Num -- Num
import GHC.Real -- Integral
import GHC.Base hiding(map, (&&), (||)) -- Eq

{- Identity function. -}
id :: a -> a
id x = x

--------------- Logical Operators -----------------
{- Logical AND (∧). -}
(&&) :: Bool -> Bool -> Bool
False && _ = False
True && right = right -- We could also do it like _ && right = right

{- Logical OR (∨). -}
(||) :: Bool -> Bool -> Bool
True || _ = True
False || right = right

----------------- List functions -----------------
-- (:) can neither be unimported nor redefined.

{- !! gives us the nth element. -}
infixl 2 !!
(!!) :: Integral a => [b] -> a -> b
(x:_) !! 0 = x
(x:xs) !! n = xs !! (n-1)

{- null checks if the list is empty. -}
null :: [a] -> Bool
null [] = True
null _ = False

{- length returns the length of a list. -}
length :: Num a => [b] -> a
length [] = 0
length (x:xs) = 1 + length xs

{- take gives us n elements from the list. -}
take :: Integral a => a -> [b] -> [b]
take _ [] = [] -- This makes take total, but incorrect definition
take 0 xs = []
take n (x:xs) = x:take (n-1) xs

{- drop deletes the n element of the list. -}
drop :: Integral a => a -> [b] -> [b]
drop _ [] = [] -- Totality hack
drop 0 xs = xs
drop n (x:xs) = drop (n-1) xs

{- elem checks if an element is on a list. -}
elem :: Eq a => a -> [a] -> Bool
elem e [] = False
elem e (x:xs) = if e == x then True else elem e xs

{- cycle repeats infinitely a list into a list. -}
cycle :: [a] -> [a]
cycle xs = xs ++ cycle xs

{- iterate creates a list by applying f to x repeatedly. -}
iterate :: (a -> a) -> a -> [a]
iterate f x = x:iterate f (f x)

{- takeWhile gets elements of a list as long as they follow a
   predicate. -}
takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ [] = []
takeWhile p (x:xs) = if p x then x:takeWhile p xs else []

{- dropWhile remove elements of a list as long as those they follow a
   predicate. -}
dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ [] = []
dropWhile p (x:xs) = if p x then takeWhile p xs else xs

{- head gives us the first element of the list, unfortunately this
   is a partial function. -}
head :: [a] -> a
head (x:xs) = x

{- tail gives us all but the first element of a list. -}
tail :: [a] -> [a]
tail [] = []
tail (x:xs) = xs

{- init gives us all but the last element of a list. -}
init :: [a] -> [a]
init [x] = []
init (x:xs) = x:init xs

{- last gives us the last element of a list, like head also partial. -}
last :: [a] -> a
last [x] = x
last (x:xs) = last xs

{- reverse reverses a list. -}
reverse :: [a] -> [a]
reverse [] = []
reverse (x:xs) = reverse xs ++ [x]

{- concat flattens a list of lists into a list. -}
concat :: [[a]] -> [a]
concat [] = []
concat (x:xs) = x ++ concat xs

{- splitAt separates the list at n. -}
splitAt :: Integral b => b -> [a] -> ([a], [a])
splitAt _ [] = ([], [])
splitAt 0 xs = ([], xs)
splitAt n (x:xs) = (x:left, right)
    where (left, right) = splitAt (n-1) xs
splitAt' n xs = (take n xs, drop n xs)

{- all checks if all the elements of a list follow a predicate. -}
all :: (a -> Bool) -> [a] -> Bool
all _ [] = True
all p (x:xs) = if p x then all p xs else False

{- any checks if any element of the lists follows a predicate. -}
any :: (a -> Bool) -> [a] -> Bool
any _ [] = False
any p (x:xs) = if p x then True else any p xs

{- and checks all elements of a list are True. -}
and :: [Bool] -> Bool
and [] = True
and (x:xs) = x && and xs
and' xs = foldl1 (&&) xs

{- or checks there is any element of a list is True. -}
or :: [Bool] -> Bool
or [] = False
or (x:xs) = x || or xs
or' xs = foldl1 (||) xs

{- nub removes duplicated elements of the list. -}
nub :: Eq a => [a] -> [a]
nub [] = []
nub (x:xs) = if elem x xs then nub xs else x:nub xs
nub' [] = []
nub' (x:xs) = x:nub' (tail (filter (/=x) xs))

{- minimum gives us the smallest element. -}
minimum :: Ord a => [a] -> a
minimum (x:y:[]) = if x < y then x else y
minimum (x:y:xs) = if x < y then minimum (x:xs) else minimum (y:xs)
minimum' :: Ord a => [a] -> a -- Haskell requires this
minimum' = foldl1 (\x y -> if x < y then x else y)

{- maximum returns the biggest element. -}
maximum :: Ord a => [a] -> a
maximum (x:y:[]) = if x > y then x else y
maximum (x:y:xs) = if x > y then maximum (x:xs) else maximum (y:xs)

{- span gives us the longest prefix that follows a predicate and the
   rest of the list. -}
span :: (a -> Bool) -> [a] -> ([a], [a])
span p xs = (takeWhile p xs, dropWhile p xs)
span' p xs = (filter p xs, filter (not . p) xs)

{- sum. -}
sum :: Num a => [a] -> a
sum [] = 0
sum (x:xs) = x + sum xs
sum' = foldl1 (+)

{- product. -}
product :: Num a => [a] -> a
product [] = 1
product (x:xs) = x * product xs
product' = foldl1 (*)

---------------------Tuples-------------------------
{- fst gives us the first element of a duple. -}
fst :: (a, b) -> a
fst (x, _) = x

{- snd gives us the second element of a duple. -}
snd :: (a, b) -> b
snd (_, x) = x

{- zip puts two lists together. -}
zip :: [a] -> [b] -> [(a, b)]
zip [] _ = []
zip _ [] = []
zip (x:xs) (y:ys) = (x, y):zip xs ys

{- unzip splits two zipped list. -}
unzip :: [(a, b)] -> ([a], [b])
unzip [(x, y)] = ([x], [y])
unzip (x:zs) = (fst x:xs, snd x:ys)
    where (xs, ys) = unzip zs
unzip' xs = ([x | (x, _) <- xs], [y | (_, y) <- xs])
unzip'' xs = (map fst xs, map snd xs)

------------ Higher Order Functions -----------------
{- map aplies a function to all elements of a list. -}
map :: (a -> b) -> [a] -> [b]
map f [] = []
map f (x:xs) = f x:map f xs

{- filters element of a list according to a predicate. -}
filter :: (a -> Bool) -> [a] -> [a]
filter p [] = []
filter p (x:xs) = if p x then x:filter p xs else filter p xs

{- reduces list by applying f till there is only one value. -}
foldl :: (a -> b -> a) -> a -> [b] -> a
foldl f s [] = s
foldl f s (x:xs) = foldl f (f s x) xs

{- foldl1 is a fold that doesn't take an initial value, but draws it
 from the list itself. -}
foldl1 :: (a -> a -> a) -> [a] -> a
foldl1 f (x:xs) = foldl f x xs
foldl1' f (x:y:[]) = f x y
foldl1' f (x:y:xs) = foldl1 f (f x y:xs)

zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith _ [] _ = []
zipWith _ _ [] = []
zipWith f (x:xs) (y:ys) = f x y:zipWith f xs ys
zipWith' f xs ys = map (\(x, y) -> f x y) (zip xs ys)

