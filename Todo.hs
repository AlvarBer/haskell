module Todo where

import System.Environment
import System.IO
import System.IO.Error
import Control.Monad

main = do 
        args <- getArgs
        let acts = handleArgs args
        forM acts (\a -> do a)

handleArgs :: [String] -> [IO ()]
handleArgs [] = []
handleArgs ("--help":xs) = putStrLn "Usage: ":handleArgs xs
handleArgs ("-h":xs) = putStrLn "Usage: ":handleArgs xs
handleArgs ("--view":xs) = view:handleArgs xs
handleArgs ("-v":xs) = view:handleArgs xs
handleArgs ("--add":xs) = [add xs]
handleArgs ("-a":xs) = [add xs]

help :: IO ()
help = do 
        putStrLn "Usage:"
        putStrLn "-h --help        show this prompt"
        putStrLn "-v --view        view all the tasks"
        putStrLn "-a --add         add a task"
        putStrLn "-d --delete      remove a task"

view :: IO ()
view = do
        withFile "todo.txt" ReadMode (\handle -> do
                contents <- hGetContents handle
                putStr contents)

add :: [String] -> IO ()
add task = do
        appendFile "todo.txt" (unlines task)

handleErrors :: IOError -> IO ()
handleErrors e = putStrLn "Wrong"

