module Labs.RPN where

solve :: (Fractional a, Read a) => String -> a
solve = head . foldl applyTerms [] . words

applyTerms :: (Fractional a, Read a) => [a] -> String -> [a]
applyTerms (x:y:xs) "+" = (x + y):xs
applyTerms (x:y:xs) "-" = (x - y):xs
applyTerms (x:y:xs) "*" = (x * y):xs
applyTerms (x:y:xs) "/" = (x / y):xs
applyTerms ts n = read n:ts

