module Labs.Lab3 where

import Control.Monad
import Data.Char(isSpace, isAlphaNum)
import Data.Function(on)
import Data.List(groupBy)
import Data.List.Split(splitOneOf)

-- We have a 2D map and the possibily of moving. How you check
-- one trayectory is always inferior to another?
data Point = Point {x :: Int, y :: Int} deriving (Eq)

origin = Point 0 0

instance Show Point where
        show (Point x y) = "(" ++ show x ++ ", " ++ show y ++ ")"

-- We bend the definition of Order, which we assign to order on the
-- Y axis, so as long Point A is higher than Point B it will be GT 
-- and viceversa, if two points have the same height (Same Y 
-- coordinate) it will be EQ
instance Ord Point where
        compare left right = compare (y left) (y right)

data Direction = North | South | East | West deriving (Show, Eq, Ord)

move :: Point -> [Direction] -> Point
move start moves = foldl (>>>) start moves

infixr 6 >>> -- Apply movement to a point
(>>>) :: Point -> Direction -> Point 
(Point x y) >>> North = Point x (y + 1)
(Point x y) >>> South = Point x (y - 1)
(Point x y) >>> East = Point (x + 1) y
(Point x y) >>> West = Point (x - 1) y

path :: Point -> [Direction] -> [Point]
path start moves = scanl (>>>) start moves

inferior :: [Direction] -> [Direction] -> Bool
inferior movs movs' = and (zipWith (<=) fstPath sndPath)
        where fstPath = path origin movs
              sndPath = path origin movs'

-- 2)
{- simpler version of complex -}
data Complex = Complex {real :: Float, imaginary :: Float}

instance Show Complex where
        show c = show (real c) ++ "-" ++ show (imaginary c) ++ "i"

-- 3)
-- Calculator lang
{- Operators -}
data Operator = Plus | Minus | Mul | Div

instance Show Operator where
        show Plus = "+"
        show Minus = "-"
        show Mul = "*"
        show Div = "/"

instance Read Operator where
        readsPrec _ "+" = [(Plus, "")]
        readsPrec _ "-" = [(Minus, "")]
        readsPrec _ "*" = [(Mul, "")]
        readsPrec _ "/" = [(Div, "")]

data Exp a = Term {operand::a, op::Operator, rest::Exp a} | 
             SingleTerm {operand::a}

instance Show a => Show (Exp a) where
        show (SingleTerm t) = show t
        show (Term t o r) = show t ++ " " ++ show o ++ " " ++ show r

instance (Read a, Fractional a) => Read (Exp a) where
        readsPrec _ input = [(fold2r buildExp "+" (SingleTerm 0) (tokenize input),"")]

tokenize :: String -> [String]
tokenize = groupBy ((==) `on` isAlphaNum) . filter (not . isSpace)

buildExp :: (Read a, Fractional a) => String -> String -> Exp a -> Exp a
buildExp term op rest = Term (read term) (read op) rest

fold2r :: (a -> a -> b -> b) -> a -> b -> [a] -> b
fold2r f i i2 (s:[]) = f s i i2
fold2r f i i2 (s:s2:xs) = f s s2 (fold2r f i i2 xs) 

eval :: Fractional a => Exp a -> a
eval (SingleTerm t) = t
eval (Term t Plus r) = t + eval r
eval (Term t Minus r) = t - eval r
eval (Term t Mul r) = t * eval r
eval (Term t Div r) = t / eval r

{-
countOperators :: Exp -> Integer
countOperators (SingleTerm t) = 0
countOperators (Term t o r) = 1 + countOperators r

changeOperators :: Exp -> Exp
changeOperators (SingleTerm t) = SingleTerm t
changeOperators (Term t Plus r) = Term t Minus (changeOperators r)
changeOperators (Term t Mul r) = Term t Div (changeOperators r)
changeOperators (Term t o r) = Term t o (changeOperators r)

operands :: Exp -> [Integer]
operands (SingleTerm t) = [t]
operands (Term t o r) = t:operands r
-}

calc :: IO()
calc = do expression <- getLine
          print("The result is " ++ show(eval(read(expression))))

-- 4)
data BTree a = Node {value::a, sons::[BTree a]} deriving (Eq)

-- To have a proper representation we need a heap (for Breadth-First traversal)
instance Show a => Show (BTree a) where
        show (Node v []) = show v
        show (Node v s) = show v ++ " {" ++ show s ++ "} "

leafList :: BTree a -> [a]
leafList (Node v []) = [v]
leafList (Node v s) = concat (map leafList s)

nodeList :: BTree a -> [a]
nodeList (Node v []) = [v]
nodeList (Node v s) = v : concat (map nodeList s)

maxTree :: (Ord a) => BTree a -> BTree a
maxTree t = replaceTree t (maximum (nodeList t))

replaceTree :: BTree a -> a -> BTree a
replaceTree (Node v []) r = Node r []
replaceTree (Node v s) r = Node r (map ((flip replaceTree) r) s)

instance Ord a => Ord (BTree a) where
        (Node t _) <= (Node t2 _) = t <= t2

-- 5)

fileWords :: String -> IO Int
fileWords fileIn = do str <- readFile fileIn
                      return (length(words str))

fileWords' :: IO ()
fileWords' = do fileIn <- getLine
                str <- fileWords fileIn
                print("File " ++ fileIn ++ " has " ++ show str ++ " words")

getInt :: IO Int
getInt = do line <- getLine
            return (read line::Int)

average :: IO()
average = average' 0 0 1

average' t a n = do int <- getInt
                    when (int /= -1) $ do
                        let total = t + int
                            avg = total `div` n
                        print("Sum: " ++ show total ++ " Average: " ++ show avg)
                        average' total avg (n+1)

{-
format :: String -> String -> Int -> IO()
format fileIn fileOut width = do 
                                contents <- readFile fileIn
                                let splitted = lines contents
                                
                                putStr splitted

justify str len
                | length str < len = words str
                | otherwise = str
-}
