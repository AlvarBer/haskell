module Labs.Marker where

import Lib

data Question = Question {value::Float, numChoices::Int, correct::Int}

data Test = Test {model::String, questions::[Question]}

data Answers = Answers {student::String, modelID::String,
                        answers::[Int]}

data Correction = Correction {studentID::String, mark::Float, 
                              markOver10::Float} deriving (Show)

data Stats = Stats {averageMark::Float, failed::Int,
                    passed::Int, good::Int, merit::Int}

check :: Test -> Answers -> Correction
check test ans = (Correction (student ans) mark mark_over10)
        where inquiries = questions test
              mark = questionsCheck inquiries (answers ans)
              mark_over10 = mark / maxMark inquiries * 10

maxMark :: [Question] -> Float
maxMark xs = foldr ((+) . value) 0 xs

questionsCheck :: [Question] -> [Int] -> Float
questionsCheck [] _ = 0
questionsCheck (q:qs) (a:as)
        | correct q == a = value q + questionsCheck qs as
        | otherwise = penalty + questionsCheck qs as
        where penalty = -1 // numChoices q - 1

getStats :: Test -> [Answers] -> Stats
getStats t as = Stats (mean marks) failed passed good merit
        where marks = map (markOver10) (map (check t) as)
              failed = len(filter (<5) marks)
              passed = len(filter (\x -> x > 5 && x < 7) marks)
              good = len(filter (\x -> x > 7 && x < 9) marks)
              merit = len(filter (>9) marks)

test = check myTest myAnswers
        where myAnswers = Answers "Juan" "01" [1, 2]
              myTest = Test "01" [q1, q2]
                where q1 = Question 15 3 1
                      q2 = Question 5 5 3

