module Labs.Lab1 where

import Data.List (foldl1')
import qualified Learn.Base as B

-- 1)
{- transform millions of years into miliseconds -}
mmYears2Milisecs mmYears = mmYears * 10e5 * 365 * 24 * 60 * 60 * 1000

{- transform seconds into years -}
secs2Years sec = sec / 60 / 60 / 24 / 365

{- seconds into (year, day, hour, min, secs) -}
dateTime :: Integral a => a -> (a, a, a, a, a)
dateTime secs = (years, days `mod` 365, hours `mod` 24, mins `mod` 60,
                 secs `mod` 60)
        where mins = secs `div` 60
              hours = mins `div` 60
              days = hours `div` 24
              years = days `div` 365

-- 2)
f x y = 2 * x - y * x
g x = f (f 2 x) (f x 1)
h x y z = f (f (x + 2 * y) (g 3)) (5 - g z - y)
i x y
        | x >= y && y > 0 = x - y
        | 0 < x && x < y = 0
        | otherwise = y - x

i' x y = if x >= y && y > 0
         then x - y
         else if x < y && x > 0
              then 0
              else y - x

-- 3)
threeEqual :: Eq a => a -> a -> a -> Bool
threeEqual x y z = x == y && y == z

pairUnequal :: Eq a => a -> a -> a -> Bool
pairUnequal x y z
        | x == y && z /= x = True
        | y == z && x /= y = True
        | x == z && y /= x = True
        | otherwise = False

threeEqual' :: Eq a => a -> a -> a -> Bool
threeEqual' x y z = (&&) ((==) x y) ((==) y z)

pairUnequal' :: Eq a => a -> a -> a -> Bool
pairUnequal' x y z
        | (&&) ((&&)((/=) x y) ((/=) y z)) ((==) x z) = True
        | (&&) ((&&)((/=) x y) ((==) y z)) ((/=) x z) = True
        | (&&) ((&&)((==) x y) ((/=) y z)) ((/=) x z) = True
        | otherwise = False

pairUnequal'' x y z = B.length (filter id [x == y, y == z, z == x]) == 1

-- 4)
-- NOT ¬ (Doen't actually works, for demostration purposes only)
infixr 5 ¬
(¬) :: Bool -> Bool
(¬) True = False
(¬) False = True

-- XOR ⊕
infixr 5 !==
(!==) :: Bool -> Bool -> Bool
True !== right = not right
False !== right = right

-- Material conditional →
infixr 2 -->
(-->) :: Bool -> Bool -> Bool
True --> False = False
_ --> _ = True

-- Equivalence ↔
infixr 2 <->
(<->) :: Bool -> Bool -> Bool
True <-> True = True
False <-> False = True
_ <-> _ = False

-- 6)
-- On Base

--7) List Functions

-- Init and Last combination
initLast :: [a] -> ([a], a)
initLast xs = (init xs, last xs)

-- Sorting Algorithms

{- Backwards bubble sort (bubble step is more straigh-forward) -}
bubbleSort :: Ord a => [a] -> [a]
bubbleSort [x] = [x]
bubbleSort xs = bubbleSort (init bubbled) ++ [last bubbled]
        where bubbled = bubble xs
              bubble :: Ord a => [a] -> [a]
              bubble (x:y:[]) = if x > y
                                then [y, x]
                                else [x, y]
              bubble (x:y:xs) = if x > y
                                then y:bubble (x:xs)
                                else x:bubble (y:xs)

{- regular ol' bubble sort -}
bubbleSort' [x] = [x]
bubbleSort' xs = head bubbled:bubbleSort' (tail bubbled)
        where bubbled = bubble' xs

bubble' xs
        | length xs == 2 = if x > y then [y, x] else [x, y]
        | otherwise =  if x > y
                       then bubble' (pxs ++ [y]) ++ [x]
                       else bubble' (pxs ++ [x]) ++ [y]
        where x = last xs
              y = (last . init) xs
              pxs = (init . init) xs

{- Selection Sort -}
selSort :: Ord a => [a] -> [a]
selSort [x] = [x]
selSort xs = minimum xs:selSort (filter (/= minimum xs) xs)

{- Insertion Sort -}
insSort :: Ord a => [a] -> [a]
insSort xs = insSortAux (length xs - 1) xs

{- TODO: Refactor this -}
insSortAux :: Ord b => Int -> [b] -> [b]
insSortAux 0 xs = insert xs
insSortAux n xs = insSortAux (n-1) (take n xs ++ insert (drop n xs))

{- Insertion step -}
insert :: Ord a => [a] -> [a]
insert [x] = [x]
insert (x:y:[]) = if x > y then [y, x] else [x, y]
insert (x:y:xs) = if x > y then y:insert (x:xs) else x:y:xs

-- Unavoidable "quick"sort
qSort :: Ord a => [a] -> [a]
qSort [] = []
qSort (x:xs) = qSort (filter (<=x) xs) ++ x:qSort (filter (>x) xs)

-- Arithmetic mean of all the list
mean :: Fractional a => [a] -> a
mean x = (sum x) / (B.length x)

-- Mean of all the lengths of a List of Lists
meanLength :: Fractional a => [[b]] -> a
meanLength xss = mean(map B.length xss)

--8) Linear Reverse
reverse' :: [a] -> [a] -> [a]
reverse' [] result = result
reverse' (x:xs) result = reverse' xs (x:result)

-- 9) Fibonacci
naivefib :: Integer -> Integer
naivefib 0 = 1
naivefib 1 = 1
naivefib n = naivefib (n-2) + naivefib (n-1)

fib :: Integral a => a -> a
fib 0 = 1
fib 1 = 1
fib n = fibaux 2 n 1 1

fibaux :: Integral a => a -> a -> a -> a -> a
fibaux current n prevprev prev
        | current < n = fibaux (current + 1) n prev (prevprev + prev)
        | otherwise = prevprev + prev

-- 10)
-- Divisors
divisors :: Integral a => a -> [a]
divisors n = filter (\x -> n `mod` x == 0) [1..n]

-- Digits
digits :: Integral a => a -> [a]
digits 0 = []
digits n = digits(n `div` 10) ++ ([n `mod` 10])

-- Number of Digits
numDigits :: Integral a => a -> a
numDigits n = B.length(digits n)

-- Check if all the digits are even
evenDigits :: Integral i => i -> Bool
evenDigits n = foldl1 (&&) (map even (digits n))

-- 11) Population Growth
-- We generate lists with the population and their growth using iterate
popGrowth :: Fractional a => a -> a -> [a]
popGrowth pop growth = iterate (\curpop -> curpop + curpop * (growth / 100)) pop

-- Like this for example
indiaGrowth = popGrowth 1267e6 0.78
chinaGrowth = popGrowth 1398e6 0.70

-- We use that list to get the desired year population
-- As you can see we do a bit of a trick to get previous-to-epoch
-- measures
predPop :: Num a => [a] -> Int -> Int -> a
predPop growList epoch year
        | year == epoch = curPop
        | year > epoch = growList !! (year - epoch)
        | year < epoch = curPop - (growList !! (epoch - year) - curPop)
                where curPop = head growList

indiaPop = predPop indiaGrowth 2014 -- Partial application
chinaPop = predPop chinaGrowth 2014

-- India & China population in 2013
population2013 = (indiaPop 2013, chinaPop 2013)
-- India & China population in 2105
population2105 = (indiaPop 2105, chinaPop 2105)
-- India & China population in 2000
population2000 = (indiaPop 2000, chinaPop 2000)
-- India & China population in 2030
population2030 = (indiaPop 2030, chinaPop 2030)

indiaGrowthPerYear = zip indiaGrowth [2014..]
chinaGrowthPerYear = zip chinaGrowth [2014..]

diffPopulation = [(x, y, year) | x <- indiaGrowth, y <- chinaGrowth, year <- [2014..]]

breakYear = diffPopulation

-- 13) Prime
prime :: Integral i => i -> Bool
prime n = B.length(divisors n) == 2

-- Inifinite Prime List
primes :: Integral a => [a]
primes = [x | x <- [1..], prime x]

-- Primes bigger than a certain number
primeBigger n = [x | x <- primes, x > n]

-- Take n Primes
nPrimes :: Integral a => a -> [a]
nPrimes n = B.take n primes

