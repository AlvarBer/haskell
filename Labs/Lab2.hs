module Labs.Lab2 where

import Prelude
import Data.List (foldl1')

import qualified Labs.Lab1 as Lab1
import Lib

-- ' means same function with different mechanisms

{- List of square roots -}
sqrtList :: (Num a, Ord a) => a -> [a]
sqrtList n
        | n > 0 = sqrtList (n - 1) ++ [n^2]
        | otherwise = []

sqrtList' :: (Num a, Enum a) => Int -> [a]
sqrtList' n = take n [x^2 | x <- [1..]]

{- List with tuples of number and respectives squares -}
sqrtListPair :: (Num a, Ord a, Num b, Enum b) => a -> [(b, a)]
sqrtListPair n = zip [1..] (sqrtList n)

{- List of powers of Three -}
-- Haven't been able to do it with simple recursion without aux parameters
powsOf3LtN :: (Num a, Ord a) => a -> [a]
powsOf3LtN n = takeWhile (<n) [3^x | x <- [0..]]

{- Get n powers of 3 -}
nPowsOf3 :: (Integral a, Num b) => a -> [b]
nPowsOf3 n = fix (\rec n -> if n == 0 then [] else rec (n-1) ++ [3^(n-1)]) n

nPowsOf3' = (flip take) (iterate (*3) 1)

nPowsOf3'' n = take n [3^x | x <- [0..]]

nPowsOf3''' n = take n (map (3^) [0..])

{- sum of the numers smaller than 1000 that divide 3 or 5 -}
sumNumLtN n = sum $ filter(\x -> x `mod` 3 == 0 || x `mod` 5 == 0) [1..n]

{- sum of the numbers smaller than 10e20 that divide 3 or 5 -}
sumNumLtBigN bigN = Lab1.sum'' [x | x <- [1..bigN], x `mod` 3 == 0 || x `mod` 5 == 0]

-- ■ lim = 20, pow = 10
powList pow lim = (map (^pow) [1..lim])

listOfPows n maxPow lim
        | n == maxPow = [powList n lim]
        | otherwise = powList n lim:listOfPows (n+1) maxPow lim

listOfTenPowers = listOfPows 1 10 20

powLoL lim pow = [map (^y) [1..lim] | y <- [1..pow]]

-- ■
powerList' [] _ = []
powerList' (x:xs) pow = (x^pow):powerList' xs (pow+1)

listOfPowers' n len maxPower
        | n == maxPower = [powerList' list 1]
        | otherwise = powerList' list 1:listOfPowers' (n+1) len maxPower
        where list = take len (cycle [n])

listOfTenPowers' = listOfPowers' 1 10 20

-- TODO: Fix this
pow2LoL pow lim = [scanl1 (\x y -> x * y) (take lim (repeat x))| x <- [1..lim]]

test t = scanl1 (\x y -> x * y) (repeat t)
-- ■
isPrime n = filter (\x -> n `mod` x == 0) [2..(n-1)] == []

primes = filter isPrime [1..]

primesLtN n = takeWhile (<n) primes

-- ■
average :: Fractional a => [a] -> a
average x = (sum x) / fromIntegral (length x)

primesAverage n = average (map fromIntegral (primesLtN n))

-- ■
primesBt200n500 = filter (>200) (takeWhile (<500) primes)

-- ■
primeGtN :: Integral n => n -> n
primeGtN n
        | isPrime n = n
        | otherwise = primeGtN (n+1)

primeGtN6923 = primeGtN 6923

-- ■
divisorsPairs = zip [19..50] (map (init . reverse . Lab1.divisors) [19..50])

-- ■
isPerfect :: Integral i => i -> Bool
isPerfect n = sum(tail(Lab1.divisors n)) == n

perfects = filter isPerfect [1..999]

{- Get the first prime followeb by 30 non primes -}
-- diffIsN 30 primesList
-- head $ diffIsN' 30 primeslist
diffIsN :: (Eq a, Num a) => a -> [a] -> a
diffIsN n (x:y:[]) = if (y - x == n) then y else 0
diffIsN n (x:y:xs) = if (y - x == n) then y else diffIsN n (y:xs)

diffIsN' :: (Eq a, Num a) => a -> [a] -> [a]
diffIsN' n = filterFold (\x y -> y - x == n)

{- filterFold is your average filter that simply takes a binary function -}
filterFold :: (a -> a -> Bool) -> [a] -> [a]
filterFold f (x:y:[]) = if f x y then [x] else []
filterFold f (x:y:xs) = if f x y then x:filterFold f (y:xs) else filterFold f (y:xs)

-- 2)
{- filter2 is a filter functions that checks 2 conditions and returns pairs of
   lists that satisfy those conditions -}
filter2 :: [a] -> (a -> Bool) -> (a -> Bool) -> ([a],[a])
filter2 xs f g = (filter f xs, filter g xs)

{- filters is the generalization of the previous function for n conditions in
   the list ps -}
filters :: [(a -> Bool)] -> [a] -> [[a]]
filters [] xs = []
filters (f:fs) xs = [filter f xs] ++ filters fs xs

filters' xs fs = [filter f xs | f <- fs]

{- A filter-like function that returns both, the elements we keep and the 
   filtered ones -}
partition :: (a -> Bool) -> [a] -> ([a], [a]) 
partition f xs = (filter f xs, filter (not . f) xs)

-- Single pass version
partition' _ [] = ([], [])
partition' f (x:xs)
        | f x = (x:matched, failed)
        | otherwise = (matched, x:failed)
        where (matched, failed) = partition' f xs

{- equivalent checks if f(x) is equal to g(x) for all n≤x≤m -}
equivalent :: (Enum a, Eq b) => (a -> b) -> (a -> b) -> a -> a -> Bool
equivalent f g n m = map f [n..m] == map g [n..m]

-- We could use foldl1 (&&) instead of the all here
equivalent' f g n m = and [f x == g x | x <- [n..m]]

-- More restricted method (Only for historical purposes)
equivalent'' :: (Integral i, Eq e) => (i -> e) -> (i -> e) -> i -> i -> Bool
equivalent'' f g n m 
        | n < m = f n == g n && equivalent f g (n+1) m 
        | n == m = f n == g m

{- Counts the number of elements that follow a condition -}
count :: (a -> Bool) -> [a] -> Int
count f xs = length(filter f xs)

count' f xs = (length . filter f) xs

{- Checks if the mayority of the list follows a condition -}
mayority :: (a -> Bool) -> [a] -> Bool
mayority p xs = count p xs > length xs `div` 2

smallerX :: Enum n => n -> n -> (n -> Bool) -> n
smallerX n m p = head (filter p [n..m])

smaller :: Enum n => n -> (n -> Bool) -> n
smaller n p = head (filter p [n..])

biggerX :: Enum n => n -> n -> (n -> Bool) -> n
biggerX n m p = head (filter p [m,pred(m)..n])

bigger :: Enum n => n -> (n -> Bool) -> n
bigger n p = head (filter p [n,pred(n)..])

ex :: (Enum n, Eq n) => n -> n -> (n -> Bool) -> Bool
ex n m p = filter p [n..m] /= []

pt :: (Enum n) => n -> n -> (n -> Bool) -> [n]
pt n m p = filter p [n..m]

-- 3) Collatz Conjecture

{- General step -}
collatz :: Integral a => a -> a
collatz n
        | n `mod` 2 == 0 = n `div` 2
        | otherwise = 3*n + 1

{- This gives us the number of steps and the intermedate steps themselves -}
collatz' :: Integral a => a -> (Int, [a])
collatz' n = (length(result), result ++ [1])
        where result = takeWhile (/=1) (iterate collatz n)

{- Finally this gives us a list of numbers from 1 till n with the number 
   of steps required to archieve collatz -}
collatz'' n = zip [1..n] (map (fst . collatz') [1..n])

-- 4)
{- Fix allows us to eliminate all recursion (Except fix itself), 
   we can think of it like passing a function itself as first argument -}
fix :: (a -> a) -> a
fix f = f (fix f) -- f(f(f(f(...))))

-- Factorial Example
fact = fix factAux
factAux f n = if n == 0 then 1 else n*f (n-1)
-- fact 3 => fix factAux 3
-- => factAux (fix factAux) 3
-- => n * (fix factAux 2)
-- => n * (factAux (fix factAux) 2
-- As you can see using fix makes it hard to reason about our functions,
-- it also tends to be worse perfomancewise, nevertheless is an 
-- interesting property of haskell.

-- foldr without explicit recursion
foldr1'' :: (a -> a -> a) -> [a] -> a
foldr1'' = fix (\rec f xs -> if length xs == 1
                             then xs !! 0
                             else head xs `f` rec f (tail (tail xs)))

-- filter without explicit recursion
filter' :: (a -> Bool) -> [a] -> [a]
filter' = fix (\rec f xs -> if null xs
                            then []
                            else if f (head xs)
                                 then head xs:rec f (tail xs)
                                 else rec f (tail xs))

-- 5)
{- All these functions are equivalent -}
f x y = x*2 - y
g x y = let z = f x y in (z, x*y, z+1)
g' x y = (f x y, x*y, f x y + 1)
g'' x y = h x y (f x y)
h x y z = (z, x*y, z+1)

f1 x y = let z = x*(y+1) in (z,x*z,z+y)
f1' x y = h1 x y (x*(y+1))
h1 x y z = (z, x*z, z+y)
f2 x y = let h z = z+x in (h 3,h x,h y)
f2' x y = h2 x y (\z -> z + x)
h2 x y z = (z 3, z x, z y)

